## Install ##

```
git clone https://bitbucket.org/sacgf/bioinfosummer_ipython.git
cd bioinfosummer_ipython
sudo pip install -r requirements.txt
jupyter notebook
```