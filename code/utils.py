'''
Created on 13/11/2016

@author: dlawrence
'''
import os
import re


def name_from_file_name(file_name):
    '''Gets file name with removing extension and directory'''
    return os.path.splitext(os.path.basename(file_name))[0]


def format_chrom(chrom, want_chr):
    ''' Pass in a chromosome (unknown format), return in your format
        @param chrom: chromosome ID (eg 1 or 'chr1')
        @param want_chr: Boolean - whether you want "chr" at the beginning of chrom
        @return: "chr1" or "1" (for want_chr True/False) 
    '''
    if want_chr:   
        if chrom.startswith("chr"):
            return chrom
        else:
            return "chr%s" % chrom
    else:
        return chrom.replace("chr", "")


def sorted_nicely(l): 
    """ Sort the given iterable in the way that humans expect.
        From http://www.codinghorror.com/blog/2007/12/sorting-for-humans-natural-sort-order.html """ 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)
