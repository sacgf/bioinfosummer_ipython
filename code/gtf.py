import HTSeq
from _collections import defaultdict
from code import utils
import sys


NORMAL_CHROMOSOMES = map(str, range(1, 23)) + ["X", "Y"]
INTRON = "intron"

def gtf_to_regions_array_of_sets(gtf_file_name):
    gff_reader = HTSeq.GFF_Reader(gtf_file_name)

    exons_by_transcript_id = defaultdict(list) 
    for feature in gff_reader:
        if feature.iv.chrom not in NORMAL_CHROMOSOMES:
            continue # Skip weird ones.

        if feature.type == 'exon':
            transcript_id = feature.attr["transcript_id"]
            exons_by_transcript_id[transcript_id].append(feature)
        #feature.attr["gene_biotype"]

    regions = HTSeq.GenomicArrayOfSets("auto", stranded=False)
    for (transcript_id, exons) in exons_by_transcript_id.iteritems():
        start = sys.maxint
        end = 0
        
        for ex in exons:
            biotype = ex.attr['gene_biotype']
            regions[ex.iv] += biotype
            
            start = min(start, ex.iv.start)
            end = max(end, ex.iv.end) 
            chrom = ex.iv.chrom # All the same...

        entire_transcript_iv = HTSeq.GenomicInterval(chrom, start, end, '.')
        regions[entire_transcript_iv] += INTRON 
        
    return regions

        
def write_genomic_array_of_sets_to_bed_file(bed_file_name, gaos):
    with open(bed_file_name, "w") as f:
        for chrom in NORMAL_CHROMOSOMES:
            unstranded_chrom_vector = gaos.chrom_vectors[chrom]['.']
            for iv, regions_set in unstranded_chrom_vector.steps():
                if regions_set:
                    # Only count introns when they're the only thing there
                    if len(regions_set) > 1 and INTRON in regions_set:
                        regions_set.remove(INTRON)
                    
                    # We need this to be sorted so names are consistent (ie not 2 counts for both "intron_protein" and "protein_intron")
                    name = '+'.join(sorted(regions_set))
                    columns = [iv.chrom, str(iv.start), str(iv.end), name]
                    f.write('\t'.join(columns) + '\n')


def create_regions_bed_file(gtf_file_name):
    name = utils.name_from_file_name(gtf_file_name)
    regions_array = gtf_to_regions_array_of_sets(gtf_file_name)
    write_genomic_array_of_sets_to_bed_file("%s_regions.bed" % name, regions_array)

if __name__ == "__main__":
    gtf_file_name = "/home/dlawrence/data/reference/genes.gtf"
    create_regions_bed_file(gtf_file_name)