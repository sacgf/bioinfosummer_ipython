from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.collections import BrokenBarHCollection
from matplotlib.figure import Figure

from code.utils import format_chrom, sorted_nicely

IDEOGRAM_FILE_NAME = "reference/cytoBand.txt"
CHROMOSOMES_ALPHA = None # 0.5
COLOR_LOOKUP = {'gneg': (1., 1., 1.),
                'gpos25': (.6, .6, .6),
                'gpos33': (.5, .5, .5),
                'gpos50': (.4, .4, .4),
                'gpos66': (.3, .3, .3),
                'gpos75': (.2, .2, .2),
                'gpos100': (0., 0., 0.),
                'acen': (.8, .4, .4),
                'gvar': (.8, .8, .8),
                'stalk': (.9, .9, .9),}

CHROMOSOME_HEIGHT = 0.9
SPACING = 0.2
WANT_CHR = True

def create_figure_and_axis():
    figure = Figure()
    figure.patch.set_facecolor('white')
    ax = figure.add_subplot(1, 1, 1)
    return figure, ax


def save_plot(figure, file_name):
    canvas = FigureCanvasAgg(figure)
    canvas.print_png(file_name)


def ideograms(fn):
    last_chrom = None
    fin = open(fn)
    fin.readline()
    xranges, colors = [], []
    ymin = 0

    for line in fin:
        chrom, start, stop, _, stain = line.strip().split('\t')
        chrom = format_chrom(chrom, WANT_CHR)
        bad_chrom = format_chrom('chrUn', WANT_CHR) 
        if chrom.startswith(bad_chrom) or chrom.endswith("random"):
            continue
        
        start = int(start)
        stop = int(stop)
        width = stop - start
        if chrom == last_chrom or (last_chrom is None):
            xranges.append((start, width))
            colors.append(COLOR_LOOKUP[stain])
            last_chrom = chrom
            continue

        ymin += CHROMOSOME_HEIGHT + SPACING
        yrange = (ymin, CHROMOSOME_HEIGHT)
        yield xranges, yrange, colors, last_chrom
        xranges, colors = [], []
        xranges.append((start, width))
        colors.append(COLOR_LOOKUP[stain])
        last_chrom = chrom

    # last one
    ymin += CHROMOSOME_HEIGHT + SPACING
    yrange = (ymin, CHROMOSOME_HEIGHT)
    yield xranges, yrange, colors, last_chrom


def sorted_ideograms(fn, **kwargs):
    chromosomes = {}
    ymin = 0
    # Calculate our own yrange (sorted order)
    for xranges, _, colors, chrom in ideograms(fn):
        chromosomes[chrom] = (xranges, colors, chrom)
    
    for key in reversed(sorted_nicely(chromosomes)):
        (xranges, colors, chrom) = chromosomes[key]
        ymin += CHROMOSOME_HEIGHT + SPACING
        yrange = (ymin, CHROMOSOME_HEIGHT)
        yield (xranges, yrange, colors, chrom)

def get_chrom_center_y(chrom, chrom_ranges):
    _, yrange = chrom_ranges[chrom]
    return yrange[0] + yrange[1]/2.

def plot_chromosomes_on_axis(ax, alpha):
    ''' returns center_y (dict of chrom Y positions) '''
    d = {}
    yticks = []
    yticklabels = []
    chrom_ranges = {}
    
    # ideogram.txt downloaded from UCSC's table browser
    for xranges, yranges, colors, label in sorted_ideograms(IDEOGRAM_FILE_NAME):
        coll = BrokenBarHCollection(xranges, yranges, facecolors=colors)
        coll.set_alpha(alpha)
        ax.add_collection(coll)
        center = yranges[0] + yranges[1]/2.
        yticks.append(center)
        yticklabels.append(label)
        d[label] = xranges
        chrom_ranges[label] = (xranges, yranges)

    ax.axis('tight')
    ax.set_yticks(yticks)
    ax.set_yticklabels(yticklabels)
    ax.yaxis.set_ticks_position('none') 
    ax.set_xticks([])
    for spine in ax.spines.values():
        spine.set_visible(False)

    center_y = {}
    for chrom in chrom_ranges:
        cy = get_chrom_center_y(chrom, chrom_ranges)
        center_y[chrom] = cy
        other_chrom = format_chrom(chrom, not WANT_CHR)
        center_y[other_chrom] = cy

    return center_y



def chromosome_plot(alpha=1.0):
    ''' Return figure, axis, chrom_ranges '''
    figure, ax = create_figure_and_axis()
    center_y = plot_chromosomes_on_axis(ax, alpha)
    return figure, ax, center_y
