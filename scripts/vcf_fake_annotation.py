#!/usr/bin/env python
'''
Created on 21/08/2014

@author: dlawrence
'''

import gzip
from os.path import basename
import sys
import vcf


SNPEFF_EFFECT = 'EFFECT_SNPEFF'
SNPEFF_GENE_NAME = 'GENE_ID_SNPEFF'
SNPEFF_IMPACT = 'IMPACT_SNPEFF'
SNPEFF_TRANSCRIPT_ID = 'TRANSCRIPT_ID_SNPEFF'

def annotate_vcf(in_vcf, out_vcf, fake_string):
    vcf_reader = vcf.Reader(open(in_vcf))
    vcf_template = vcf.Reader(open(in_vcf)) # Reuse existing header
    
    if out_vcf.endswith(".gz"):
        f = gzip.open(out_vcf, "w")
    else:
        f = open(out_vcf, "w")
        
    vcf_writer = vcf.Writer(f, vcf_template)

    for v in vcf_reader:
        v.INFO[SNPEFF_EFFECT] = fake_string
        v.INFO[SNPEFF_GENE_NAME] = 'ENSG00000228327'
        v.INFO[SNPEFF_IMPACT] = fake_string
        v.INFO[SNPEFF_TRANSCRIPT_ID] = 'ENST00000428504'
        vcf_writer.write_record(v)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        sys.stderr.write("Usage %s: in.vcf out.vcf FAKE_STRING\n" % basename(sys.argv[0]))
        sys.exit(1)

    annotate_vcf(*tuple(sys.argv[1:]))