#!/usr/bin/env python
'''
16 November 2016

Paul Wang (paul.wang@sa.gov.au)





'''

import vcf
from numpy.random import normal, random



# constrain AF value to be within 0 and 1
def trim_AF(af_value):
    while af_value > 1:
        af_value = 1.0 - af_value
    while af_value < 0:
        af_value = abs(af_value)
    return af_value




#

# VCFDATA = "data_dont_commit/ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf.gz"
VCFDATA = "data/AF_data.vcf.gz"
invcf = vcf.Reader(filename=VCFDATA)

HET_RATIO = 0.4    # fraction of sites which are heterozygous

CNV_AF = 0.7     # normal het AF is 0.5, CNF_AF is set to be 0.666 for simplicity sake

outvcf = open("data/AF_data.v2.vcf", "w")

header = """##fileformat=VCFv4.1
##fileDate=20161121
##reference=ftp://ftp.1000genomes.ebi.ac.uk//vol1/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz
##source=1000GenomesPhase3Pipeline,modified
##contig=<ID=1,assembly=b37,length=249250621>
##contig=<ID=2,assembly=b37,length=243199373>
##contig=<ID=3,assembly=b37,length=198022430>
##contig=<ID=4,assembly=b37,length=191154276>
##contig=<ID=5,assembly=b37,length=180915260>
##contig=<ID=6,assembly=b37,length=171115067>
##contig=<ID=7,assembly=b37,length=159138663>
##contig=<ID=8,assembly=b37,length=146364022>
##contig=<ID=9,assembly=b37,length=141213431>
##contig=<ID=10,assembly=b37,length=135534747>
##contig=<ID=11,assembly=b37,length=135006516>
##contig=<ID=12,assembly=b37,length=133851895>
##contig=<ID=13,assembly=b37,length=115169878>
##contig=<ID=14,assembly=b37,length=107349540>
##contig=<ID=15,assembly=b37,length=102531392>
##contig=<ID=16,assembly=b37,length=90354753>
##contig=<ID=17,assembly=b37,length=81195210>
##contig=<ID=18,assembly=b37,length=78077248>
##contig=<ID=19,assembly=b37,length=59128983>
##contig=<ID=20,assembly=b37,length=63025520>
##contig=<ID=21,assembly=b37,length=48129895>
##contig=<ID=22,assembly=b37,length=51304566>
##contig=<ID=GL000191.1,assembly=b37,length=106433>
##contig=<ID=GL000192.1,assembly=b37,length=547496>
##contig=<ID=GL000193.1,assembly=b37,length=189789>
##contig=<ID=GL000194.1,assembly=b37,length=191469>
##contig=<ID=GL000195.1,assembly=b37,length=182896>
##contig=<ID=GL000196.1,assembly=b37,length=38914>
##contig=<ID=GL000197.1,assembly=b37,length=37175>
##contig=<ID=GL000198.1,assembly=b37,length=90085>
##contig=<ID=GL000199.1,assembly=b37,length=169874>
##contig=<ID=GL000200.1,assembly=b37,length=187035>
##contig=<ID=GL000201.1,assembly=b37,length=36148>
##contig=<ID=GL000202.1,assembly=b37,length=40103>
##contig=<ID=GL000203.1,assembly=b37,length=37498>
##contig=<ID=GL000204.1,assembly=b37,length=81310>
##contig=<ID=GL000205.1,assembly=b37,length=174588>
##contig=<ID=GL000206.1,assembly=b37,length=41001>
##contig=<ID=GL000207.1,assembly=b37,length=4262>
##contig=<ID=GL000208.1,assembly=b37,length=92689>
##contig=<ID=GL000209.1,assembly=b37,length=159169>
##contig=<ID=GL000210.1,assembly=b37,length=27682>
##contig=<ID=GL000211.1,assembly=b37,length=166566>
##contig=<ID=GL000212.1,assembly=b37,length=186858>
##contig=<ID=GL000213.1,assembly=b37,length=164239>
##contig=<ID=GL000214.1,assembly=b37,length=137718>
##contig=<ID=GL000215.1,assembly=b37,length=172545>
##contig=<ID=GL000216.1,assembly=b37,length=172294>
##contig=<ID=GL000217.1,assembly=b37,length=172149>
##contig=<ID=GL000218.1,assembly=b37,length=161147>
##contig=<ID=GL000219.1,assembly=b37,length=179198>
##contig=<ID=GL000220.1,assembly=b37,length=161802>
##contig=<ID=GL000221.1,assembly=b37,length=155397>
##contig=<ID=GL000222.1,assembly=b37,length=186861>
##contig=<ID=GL000223.1,assembly=b37,length=180455>
##contig=<ID=GL000224.1,assembly=b37,length=179693>
##contig=<ID=GL000225.1,assembly=b37,length=211173>
##contig=<ID=GL000226.1,assembly=b37,length=15008>
##contig=<ID=GL000227.1,assembly=b37,length=128374>
##contig=<ID=GL000228.1,assembly=b37,length=129120>
##contig=<ID=GL000229.1,assembly=b37,length=19913>
##contig=<ID=GL000230.1,assembly=b37,length=43691>
##contig=<ID=GL000231.1,assembly=b37,length=27386>
##contig=<ID=GL000232.1,assembly=b37,length=40652>
##contig=<ID=GL000233.1,assembly=b37,length=45941>
##contig=<ID=GL000234.1,assembly=b37,length=40531>
##contig=<ID=GL000235.1,assembly=b37,length=34474>
##contig=<ID=GL000236.1,assembly=b37,length=41934>
##contig=<ID=GL000237.1,assembly=b37,length=45867>
##contig=<ID=GL000238.1,assembly=b37,length=39939>
##contig=<ID=GL000239.1,assembly=b37,length=33824>
##contig=<ID=GL000240.1,assembly=b37,length=41933>
##contig=<ID=GL000241.1,assembly=b37,length=42152>
##contig=<ID=GL000242.1,assembly=b37,length=43523>
##contig=<ID=GL000243.1,assembly=b37,length=43341>
##contig=<ID=GL000244.1,assembly=b37,length=39929>
##contig=<ID=GL000245.1,assembly=b37,length=36651>
##contig=<ID=GL000246.1,assembly=b37,length=38154>
##contig=<ID=GL000247.1,assembly=b37,length=36422>
##contig=<ID=GL000248.1,assembly=b37,length=39786>
##contig=<ID=GL000249.1,assembly=b37,length=38502>
##contig=<ID=MT,assembly=b37,length=16569>
##contig=<ID=NC_007605,assembly=b37,length=171823>
##contig=<ID=X,assembly=b37,length=155270560>
##contig=<ID=Y,assembly=b37,length=59373566>
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=VAF,Number=1,Type=Float,Description="Variant allele frequency">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	Sample1	Sample2
"""

outvcf.write(header)

var_count = 0

for var in invcf:

    # filter by variant type
    if var.is_snp == False:
        continue

    # filter by number of ALT
    if len(var.ALT) > 1:
        continue

    # filter by AF
    # only if using original 1KG file
    # if var.INFO["AF"][0] < 0.45 or var.INFO["AF"][0] > 0.55:
    #     continue

    # skip Y chromosome and the rest
    if var.CHROM == "Y":
        break

    var_count += 1

    # generate random AF
    sample_AF = []
    sample_GT = []
    for s in range(2):

        # --------------------------------------------
        # is variant in CNV region?
        is_CNV = False
        is_LOH = False

        # Sample1 CNV
        # chrom 4, 150M onward
        # chrom 16, 20M - 40M

        if s == 0:
            if var.CHROM == "4" and var.POS > 150485264:
                is_CNV = True
            elif var.CHROM == "16" and var.POS > 20865729 and var.POS < 40584930:
                is_CNV = True
            elif var.CHROM == "12":
                if ((var.POS >= 12059607 and var.POS <= 18093059) or
                    (var.POS >= 24859609 and var.POS <= 38493059) or
                    (var.POS >= 65902839 and var.POS <= 81029384)):
                    is_LOH = True

        # Sample2 CNV
        # entire chrom 6
        # chrom 18, multiple regions
        #    (10 - 11.5)M
        #    (13.6 - 14)M
        #    (15 - 15.6)M
        #    (16.5 - 17.9)M
        #    (23 - 25)M
        #    (28.5 - 31)M
        #    (41 - 44.5)M
        #    (60 - 62)M
        elif s == 1:
            if var.CHROM == "6":
                is_CNV = True
            elif var.CHROM == "18":
                if ( (var.POS >= 10574839 and var.POS <= 11594039) or
                     (var.POS >= 13695840 and var.POS <= 14849302) or
                     (var.POS >= 15048375 and var.POS <= 15638293) or
                     (var.POS >= 16532193 and var.POS <= 17939284) or
                     (var.POS >= 23184930 and var.POS <= 25102938) or
                     (var.POS >= 28532409 and var.POS <= 31029485) or
                     (var.POS >= 41274830 and var.POS <= 44598283) or
                     (var.POS >= 60019283 and var.POS <= 62172834) ):
                    is_CNV = True
            elif var.CHROM == "20":
                is_LOH = True

        # --------------------------------------------
        # is variant heterozygous?
        is_het = False
        het_prob = random()
        if het_prob < HET_RATIO:
            is_het = True

        # --------------------------------------------
        # if homozygous
        if not is_het:
            af_ = normal(loc=1.0, scale=0.02)
            af_ = trim_AF(af_)
            sample_AF.append(af_)

            # if af_ is closer to 1
            if (1-af_) < af_:
                sample_GT.append("1/1")
            else:
                sample_GT.append("0/0")


        # --------------------------------------------
        # if heterozygous
        else:
            if is_CNV:
                af_ = normal(loc=CNV_AF, scale=0.04)
                af_ = trim_AF(af_)

                # whether to flip the AF value
                flip_af = random()
                if flip_af > 0.5:
                    af_ = 1 - af_
            elif is_LOH:
                af_ = normal(loc=0, scale=0.02)
                af_ = trim_AF(af_)
                sample_AF.append(af_)
            else:
                af_ = normal(loc=0.5, scale=0.02)
                af_ = trim_AF(af_)

            sample_AF.append(af_)
            sample_GT.append("0/1")

    # Write output
    vcfline = "%s\t%d\t%s\t%s\t%s\t.\t.\t.\tGT:VAF" % (var.CHROM, var.POS,
               var.ID, var.REF, var.ALT[0].sequence)

    vcfline += "\t%s:%f\t%s:%f" % (sample_GT[0], sample_AF[0], sample_GT[1], sample_AF[1])
    outvcf.write(vcfline + "\n")

    # reporting progress
    if var_count % 100 == 0:
        print "written %d variants\t\t%s" % (var_count, vcfline)


outvcf.close()
