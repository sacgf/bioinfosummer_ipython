#!/usr/bin/env python

from code.graphing import chromosome_plot, plot_chromosomes_on_axis, CHROMOSOME_HEIGHT
from code import gtf
from code.utils import format_chrom
from collections import Counter
from matplotlib.collections import PatchCollection
from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle
import gzip
import HTSeq
import pandas as pd
import seaborn as sns
import sys
import vcf

# Need latest Pandas
df = pd.DataFrame.from_records([{"foo" : 42}])
df.sort_values("foo")

print "Hello, world!"
