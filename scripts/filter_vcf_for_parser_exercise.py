#!/usr/bin/env python
'''
17 November 2016

Paul Wang (paul.wang@sa.gov.au)

Filtering a CML multi-sample VCF file down to just a few variants that can be used for
demonstrating pyvcf parser.

Should have:
- both indels and SNVs
- with and without QUAL (failed)
- various FILTER levels
- genotype
- spread over all chromsomes



'''

import vcf


VCFDATA = "data_dont_commit/ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf.gz"



invcf = vcf.Reader(filename=VCFDATA)
