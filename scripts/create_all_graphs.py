#!/usr/bin/env python
'''
Created on 13/11/2016

@author: dlawrence
'''
from code.graphing import chromosome_plot, save_plot


def plot_chrom_test():
    figure, ax, center_y = chromosome_plot(0.2)

    variants = [('chr1',  51000000, 'b'),
                ('chr5',  95000000, 'g'),
                ('chr12', 13000000, 'r'),
    ]

    x = []
    y = []
    colors = []
    for (chrom, position, color) in variants:
        x.append(position)
        y.append(center_y[chrom])
        colors.append(color)
        
    ax.scatter(x, y, c=colors)
    save_plot(figure, "chrom_graph.png")


def main():
    plot_chrom_test()


if __name__ == '__main__':
    main()